#!/usr/bin/env python
# -*- coding: utf-8 -*- 

__author__ = 'Chengwei Luo (luo.chengwei@gatech.edu)'
__version__ = '0.1.0'
__date__ = 'Dec 2015'

"""
metaHGT: Horizontal Gene Transfer (HGT) detecting with series metagenomics

Copyright(c) 2015 Chengwei Luo (luo.chengwei@gatech.edu)

	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>

https://bitbucket.org/luo-chengwei/metaHGT

This script is part of the metaHGT package,

for help for metaHGT main function, type:
python metaHGT.py --help

"""

USAGE = \
"""Usage: %prog <required_parameters> [options]

metaHGT: Horizontal Gene Transfer (HGT) detecting with series metagenomics

Add --help to see a full list of required and optional
arguments to run metaHGT.

Additional information can also be found at:
https://bitbucket.org/luo-chengwei/metaHGT/wiki

If you use metaHGT in your work, please cite it as:
<metaHGT citation TBD>

Copyright: Chengwei Luo, Georgia Institute of Technology, 2015
"""


import sys, os, re, glob, shutil, math
from optparse import OptionParser, OptionGroup
from subprocess import call, PIPE, Popen
import multiprocessing as mp
from time import ctime, time
from datetime import timedelta
import cPickle
from itertools import product, combinations
from operator import itemgetter, attrgetter

import numpy as np
from scipy import stats
import pandas as pd
import networkx as nx

from Bio import SeqIO
from Bio.Seq import Seq

import pysam


########################### classes #########################
class PE_linkage:
	def __init__(self):
		self.ref_sample = None
		self.nodes = [None, None]
		self.linkage_num = [0, 0]
		self.locs = [[], []]
		
	def add_link(self, info):
		if info['mapping'][0] != self.nodes[0]: return 0
		if info['samples'][0] == self.ref_sample:
			self.linkage_num[0] += 1
			self.locs[0].append((info['mapping'][1], info['mapping'][2], \
								 info['mapping'][4], info['mapping'][5]))
		else:
			self.linkage_num[1] += 1
			self.locs[1].append((info['mapping'][1], info['mapping'][2], \
								 info['mapping'][4], info['mapping'][5]))
		return 0
		
	def est_loc(self, lib_property, samples):
		if len(self.locs[0]) == 0: return None, None, (None, None)  # trivial case
		s, sd, rlen = lib_property[self.ref_sample]
		pos1 = int(float(sum(map(itemgetter(0), self.locs[0])))/len(self.locs[0]))
		pos2 = int(float(sum(map(itemgetter(2), self.locs[0])))/len(self.locs[0]))
		if self.locs[0][0][1] == '+': pos1 += int(0.5*s)
		else: pos1 -= int(0.5*s) - int(rlen)
		if self.locs[0][0][3] == '-': pos2 -= int(0.5*s) - int(rlen)
		else: pos2 += int(0.5*s)
		orientation = [self.locs[0][0][1], self.locs[0][0][3]]
		return pos1, pos2, orientation

class Coverage:
	def __init__(self):
		self.contig = None
		self.start = 0
		self.end = 0
		self.cov = {}

class HGT:
	def __init__(self):
		self.indel = 1 # 1: insertion, 0: deletion
		self.insert_segments = []
		self.segments_orientation = []
		self.left_neighbor = None
		self.right_neighbor = None
		self.break_points = [[0, 0], [0, 0]]
		self.orientations = [None, None, None]
		self.base_cov_ratio = [0, 0]
		self.population_perc = 0
		self.supporting_PEs = [0, 0]
		self.cov = None
		self.cov_sd = None
		self.pvalue = 1
		
########################### utility functions #########################

def which(program):
	def is_exe(fpath):
		return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

	fpath, fname = os.path.split(program)
	if fpath:
		if is_exe(program):
			return program
	else:
		for path in os.environ["PATH"].split(os.pathsep):
			path = path.strip('"')
			exe_file = os.path.join(path, program)
			if is_exe(exe_file):
				return exe_file
	return None

def parse_conf(conf):
	samples = []
	assembly = ''
	binfile = ''
	depthfile = ''
	bamfiles = {}
	if conf == None or not os.path.exists(conf):
		sys.stderr.write('[FATAL]: cannot find the configuration file.\n')
		exit(1)
	else:
		try:
			with open(conf, 'rb') as cfh:
				for line in cfh:
					keyword, value = line.rstrip().split('\t')
					if keyword == 'assembly':
						assembly = os.path.abspath(value)
					elif keyword == 'bins':
						binfile = os.path.abspath(value)
					elif keyword == 'depths':
						depthfile = os.path.abspath(value)
					else:
						samples.append(keyword)
						bamfiles[keyword] = os.path.abspath(value)
			
			# verify all the files			
			if not os.path.exists(assembly):
				sys.stderr.write("[FATAL]: failure in locating assembly file: %s\n" % assembly)
				exit(1)
			
			if not os.path.exists(depthfile):
				sys.stderr.write("[FATAL]: failure in locating contig depth file: %s\n" % depthfile)
				exit(1)
				
			for sample in samples:
				bamfile = bamfiles[sample]
				baifile = bamfile+'.bai'
				if not os.path.exists(bamfile):
					sys.stderr.write("[FATAL]: failure in locating BAM file: %s\n" % bamfile)
				if not os.path.exists(baifile):
					sys.stderr.write("[FATAL]: failure in locating BAM index file: %s\n" % baifile)
		except:
			sys.stderr.write('[FATAL]: Error in parsing the configuration file.\n')
			exit(1)
	
	return assembly, binfile, depthfile, samples, bamfiles

########################### algorithms #########################

def estimate_lib(samples, bamfiles, len_dict):
	lib_property = {}
	for sample in samples:
		bamfile = pysam.AlignmentFile(bamfiles[sample], 'rb')
		lib_sizes = []
		read_sizes = []
		num_reads = 0
		num_proper_pair = 0
		for ref in bamfile.references:
			for read in bamfile.fetch(reference = ref, start = 0, end = len_dict[ref]):
				if read.is_secondary or read.is_supplementary or read.mapq < 15: continue
				num_reads += 1
				if not read.is_proper_pair: continue
				lib_sizes.append(abs(read.pos - read.mpos) + read.qlen + 1)
				read_sizes.append(read.qlen)
				num_proper_pair += 1
			if len(lib_sizes) > 1e5: break
		lib_property[sample] = [np.mean(lib_sizes), np.std(lib_sizes), np.mean(read_sizes)]
		bamfile.close()
	return lib_property
	
def get_coverage(depthfile, samples):
	df = pd.read_csv(depthfile, sep = '\t', index_col = 0)
	avg_cov_ind = range(2, 2*len(samples)+1, 2)
	cov_var_ind = range(3, 2*len(samples)+1, 2)
	contig_coverage_df = df.ix[:, avg_cov_ind]
	sample_name_dict = dict(zip(contig_coverage_df.columns.values, samples))
	contig_coverage_df.rename(columns=sample_name_dict, inplace=True)
	contig_variance_df = df.ix[:, cov_var_ind]
	sample_name_dict = dict(zip(contig_variance_df.columns.values, samples))
	contig_variance_df.rename(columns=sample_name_dict, inplace=True)
	return contig_coverage_df, contig_variance_df
		
def run_PE_info_prep(args):
	bamfile, out_pkl, len_dict, contig_bin_dict, reference_dict, lib_property, sample = args
	lib_size, lib_sd, lib_rlen = lib_property[sample]
	bam = pysam.AlignmentFile(bamfile, 'rb')
	sys.stdout.write('      [%s] Parsing BAM file, identify cross bin linkages\n' % sample)
	regions = []
	linkages = []
	for read in bam.fetch():
		if read.is_secondary or read.mapq<15: continue # skip if low quality mapping or secondary
		if read.reference_id == read.next_reference_id:
			if read.is_read2: continue
			if read.pos < read.mpos:
				x_insert_size = read.mpos + lib_rlen - read.pos + 1
			else:
				x_insert_size = read.pos + lib_rlen - read.mpos + 1
				
			if x_insert_size > lib_size+5*lib_sd: 
				s, e = min(read.pos, read.mpos), max(read.mpos, read.pos)
				contig = reference_dict[read.reference_id]
				regions.append([contig, s, e])
		else:
			r1 = reference_dict[read.reference_id]
			try: r2 = reference_dict[read.next_reference_id]
			except: continue
			if read.is_read1: c1, c2 = '1', '2'
			else: c1, c2 = '2', '1'
			# make orientation calls
			if c1 == '1' and not read.is_reverse: o1 = 'R'
			elif c1 == '1' and read.is_reverse: o1 = 'L'
			elif c1 == '2' and not read.is_reverse: o1 = 'L'
			else: o1 = 'R'
			if c2 == '1' and not read.mate_is_reverse: o2 = 'R'
			elif c2 == '1' and read.mate_is_reverse: o2 = 'L'
			elif c2 == '2' and not read.mate_is_reverse: o2 = 'L'
			else: o2 = 'R'
			linkages.append([(r1, c1, read.pos, o1), (r2, c2, read.mpos, o2)])
	bam.close()
	cPickle.dump([regions, linkages], open(out_pkl, 'wb'))
	sys.stdout.write('      [%s] Done.\n' % sample)
	return 0

def identify_regions(x):
	regions = []
	# identify regions as segments with no overlap
	G = nx.MultiGraph()
	G.add_nodes_from(range(len(x)))
	for i, (s1, e1) in enumerate(x):
		for j, (s2, e2) in enumerate(x):
			if i >= j: continue
			if s1 > e2 or e1 < s2: continue
			G.add_edge(i, j)
	
	for component in nx.connected_components(G):
		regions.append([])
		for ind in component:
			s, e = x[ind]
			regions[-1].append([s, e])
	R = []
	for x in regions:
		start = min(map(itemgetter(0), x))
		end = max(map(itemgetter(1), x))
		R.append([start, end])
	return R

def find_region_key(contig, s, e, regions):
	for c, start, end in regions:
		if c != contig: continue
		if s >= start and e <= end: return (c, start, end)
	return None

def identify_locs(x, resolution = 300):
	# 1-d division of list
	L = []
	sorted_x = sorted(x)
	inds = []
	for i in range(len(x)-1):
		if sorted_x[i+1]-sorted_x[i] > resolution: inds.append(i)
	inds.insert(0, 0)
	inds.insert(len(x), len(x)-1)
	for s_ind, e_ind in zip(inds[:-1], inds[1:]):
		L.append(sorted_x[s_ind: e_ind+1])
	locs = []
	for y in L: locs.append(int(np.mean(y)))
	return locs

def find_loc_key(locs, a, resolution = 300):
	for b in locs:
		if abs(a-b) < resolution: return b
	return None

def aggregate_info(samples, options):
	PE_info_dir = '%s/PE_info.tmp' % os.path.abspath(options.outdir)
	candidate_regions = {}
	linkages = {}
	for sample in samples:
		sample_PE_pkl = '%s/%s.PE_info.pkl' % (PE_info_dir, sample)
		if not os.path.exists(sample_PE_pkl):
			sys.stderr.write('[FATAL]: Error in processing PE info for sample: %s\n' % sample)
			exit(1)
		sample_regions, sample_linkages = cPickle.load(open(sample_PE_pkl, 'rb'))
		candidate_regions[sample] = sample_regions
		linkages[sample] = sample_linkages
		del sample_regions
		del sample_linkages
			
	# aggregate results
	sys.stdout.write('    Aggregating results....\n')
	# aggregate regions first
	sys.stdout.write('      [+] Aggregating regions....\n')
	agg_regions = {}
	X = {}
	for sample in samples:
		for contig, s, e in candidate_regions[sample]:
			if contig not in X: X[contig] = []
			X[contig].append((s, e))
	for contig in X:
		regions = identify_regions(X[contig])
		for s, e in regions:
			agg_regions[(contig, s, e)] = {}
			for sample in samples: agg_regions[(contig, s, e)][sample] = 0
	# iterate again to count per sample
	for sample in samples:
		for contig, s, e in candidate_regions[sample]:
			K = find_region_key(contig, s, e, agg_regions.keys())
			if K == None: continue
			agg_regions[K][sample]+=1
		
	# then aggregate the linkages
	sys.stdout.write('      [+] Aggregating linkages....\n')
	G = nx.Graph()
	# init the nodes
	# identify regions/hotspots
	hotspots = {} # keyed by contigs, valued as list of locs
	for sample in linkages:
		for (r1, c1, p1, o1), (r2, c2, p2, o2) in linkages[sample]:
			if r1 not in hotspots: hotspots[r1] = []
			hotspots[r1].append(p1)
			if r2 not in hotspots: hotspots[r2] = []
			hotspots[r2].append(p2)
	# get locs from hotspots
	x_locs = {}
	for r in hotspots:
		locs = identify_locs(hotspots[r])
		x_locs[r] = locs
	# init node and edges for agg_linkages
	G.add_nodes_from(x_locs.keys())
	edges = []
	for sample in linkages:
		for (r1, c1, p1, o1), (r2, c2, p2, o2) in linkages[sample]:
			l1 = find_loc_key(x_locs[r1], p1)
			l2 = find_loc_key(x_locs[r2], p2)
			if l1 == None or l2 == None: continue
			# init the edge
			if r1 not in G[r2]:
				G.add_edge(r1, r2, info=[])
			# update the edge
			G[r1][r2]['info'].append([sample, (r1, l1, c1, p1, o1), (r2, l2, c2, p2, o2)])
				
	# collapse to multi-edge graph
	agg_linkages = nx.MultiGraph()
	agg_linkages.add_nodes_from(x_locs.keys())
	for contig1, contig2 in G.edges():
		x = {}
		for sample, (r1, l1, c1, p1, o1), (r2, l2, c2, p2, o2) in G[contig1][contig2]['info']:
			if r1 == contig1: k = (l1, l2)
			else: k = (l2, l1)
			if k not in x:
				x[k] = {}
				if r1 == contig1: ori = (o1, o2)
				else: ori = (o2, o1)
				for s in samples: x[k][s] = {'orientation': ori, 'count': 0}
			x[k][sample]['count'] += 1 
		for l1, l2 in x: # add_links
			agg_linkages.add_edge(contig1, contig2, 
						locs = (contig1, l1, contig2, l2), details = x[(l1, l2)])
						
	return agg_regions, agg_linkages

def cal_segment_coverage(bam, lib_property, c, s, e):
	rlen, insert_size, insert_sd = lib_property
	mpos = []
	for read in bam.fetch(c, start = s, end = e):
		if read.is_secondary or read.mapq < 15: continue
		mpos.append(read.mpos)
	mpos = sorted(mpos)
	covs = []
	for ind in range(s, e, 100): covs.append(0)
	for pos in mpos:
		try: covs[(pos-1)/100]+=1
		except: continue
	avg_cov = float(np.median(covs)*rlen)/100
	return avg_cov

def cal_PE_prob(x, background_br):
	mean, std = background_br
	r = float(x[0])/sum(x)
	p = 1-stats.norm.cdf(r, loc = mean, scale = std)
	return p

def get_cov(cov, contig, s, e, sample):
	overlaps = {}
	for start, end in cov[contig]:
		x = max(start, s)
		y = min(end, e)
		overlaps[(start, end)] = y-x+1
	os, oe = sorted(overlaps.iteritems(), key = lambda a: a[1], reverse = True)[0][0]
	return cov[contig][(os, oe)][sample]

def translate_to_string(path):
	p = []
	for contig, loc in path:
		p.append('%s|%i' % (contig, loc))
	return ','.join(p)

def find_other_loc(seg_cov, s):
	for seg_s, seg_e in seg_cov:
		if s==seg_s: return seg_e
		if s==seg_e: return seg_s
	return -1

def graph_HGT(G, min_perc, pval):
	H_nodes = [x for x, info in G.nodes(data=1) if info['type'] == 'H']
	B_nodes = [x for x in G.nodes() if x not in H_nodes]
	if len(H_nodes) == 0 or len(B_nodes) == 0: return []
	
#	for for x1, x2, info in G.edges(data=1): print x1, x2, info
#	for node, node_prop in G.nodes(data=1): print node, node_prop
#	for n1, n2, edge_prop in G.edges(data=1): print n1, n2, G[n1][n2]
	# iterate through the H_nodes, cut off the contig edge and explore all the paths
	H_contig_edges = [(x1, x2) for x1, x2 in G.edges()
					if G[x1][x2]['type'] == 'contig' and x1 in H_nodes and x2 in H_nodes]
	PE_edges = [(x1, x2) for x1, x2 in G.edges() if G[x1][x2]['type'] == 'PE']
	
	M = G.copy()
	edges_to_remove = []
	edges_to_add = []
	direction = '+'
	for x1, x2 in PE_edges:
		counts = G[x1][x2]['counts']
		covs = [G.node[x1]['covs'], G.node[x2]['covs']]
		types = [G.node[x1]['type'], G.node[x2]['type']]
		if types[0] == 'B' and types[1] == 'H': # reverse everything
			x1, x2 = x2, x1
			covs = covs[::-1]
			direction = '-'
		elif types[0] == 'H' and types[1] == 'B': pass
		else: continue
		# run the test for odds/perc and pval
		if min(sum(covs[0]), sum(covs[1]), sum(counts)) < 5 \
			or covs[1][1] == 0 or covs[1][0] == 0: continue # trivial case, skip
		B_ratio = covs[1][0]/sum(covs[1])
		H_ratio = covs[0][0]/sum(covs[0])
		if covs[0][0] == 0: perc = 100
		else: perc = 100*(covs[0][1]/covs[0][0])*H_ratio
		
		if perc < min_perc: continue
		odds_ratio, p1 = stats.fisher_exact(covs)
		if p1 > 0.05: continue
		S = sum(counts)
		#ratio = sum(covs[0])/(sum(covs[0])+sum(covs[1]))
		simu_num = [max(0, S-int(B_ratio*S)), int(B_ratio*S)] # simulate the background ratio
		o, p2 = stats.fisher_exact([simu_num, counts])
		if p2 > pval or o < 1: continue
		edges_to_add.append([x1, x2, p2, perc, direction, G[x1][x2]['orientations']])
	if len(edges_to_add) == 0: return []
	# cut PE connection
	M.remove_edges_from([(x1, x2) for x1, x2 in G.edges() if G[x1][x2]['type'] == 'PE'])
	for x1, x2, p, perc, direction, orientations in edges_to_add:
		M.add_edge(x1, x2, pvalue = p, percentage = perc, 
				direction = direction, orientations = orientations)
	
	# remove all edges that are PE but do not qualify
	all_paths = []
	for x1, x2 in H_contig_edges:
		contig = x1[0]
		H = M.copy()
		H.remove_edge(x1, x2)  
		leaves = [x for x in H.nodes() if x!=x1 and x!=x2 and H.degree(x) == 1]
		paths = {x1:[], x2:[]}
		# x1's paths:
		for leaf in leaves:
			paths[x1]+=list(nx.all_simple_paths(H, x1, leaf))
			paths[x2]+=list(nx.all_simple_paths(H, leaf, x2))
		# 
		# get all the paths
		x_paths = []
		if paths[x1] == []:
			for path2 in paths[x2]:
				if path2[0][0] == contig:
					path = [x1]+path2
					if x1[1] > path2[0][1]: path = path[::-1] # flip it
				else:
					path = path2+[x1]
					if path2[-1][1] > x1[1]: path = path[::-1]
				x_paths.append(path)
		elif paths[x2] == []:
			for path1 in paths[x1]:
				if path1[0][0] == contig:
					path = [x2]+path1
					if x2[1] > path1[0][1]: path = path[::-1]
				else:
					path = path1+[x2]
					if path1[-1][1] > x2[1]: path = path[::-1]
				x_paths.append(path)
		else:
			for path1, path2 in list(product(paths[x1], paths[x2])):
				if path1[0][0] == contig:
					path = path2+path1
					if path1[0][1] < path2[-1][1]: path = path[::-1] # flip it
				else:
					path = path1+path2
					if path1[-1][1] > path2[0][1]: path = path[::-1]
				if len(path) != len(set(path)): continue
				x_paths.append(path)
		
		# interpret each path		
		for path in x_paths:	
			path_st = translate_to_string(path)
			HGT_part = []
			part_cov = {}
			path_details = {}
			for n1, n2 in zip(path[:-1], path[1:]):
				n1_st = '%s|%i' % (n1[0], n1[1])
				n2_st = '%s|%i' % (n2[0], n2[1])
				if G[n1][n2]['type'] == 'contig':
					coverage = G.node[n1]['covs']
					start_loc, end_loc = sorted([n1[1], n2[1]])
					seg_tag = '%s|%i-%i' % (n1[0], start_loc, end_loc)
					part_cov[seg_tag] = coverage
				if n1 in H_nodes and n2 in H_nodes:
					if n1[1] > n2[1]: HGT_part.append('%s,%s' % (n2_st, n1_st))
					else: HGT_part.append('%s,%s' % (n1_st, n2_st))
					continue
				if 'pvalue' not in M[n1][n2]: continue
				pvalue = M[n1][n2]['pvalue']
				percentage = M[n1][n2]['percentage']
				orientations = M[n1][n2]['orientations']
				pvalue = M[n1][n2]['pvalue']
				loc = orientations[0]
				node = '%s|%i,%s|%i' % (loc[0], loc[1], loc[2], loc[3])
				if path_st.count(node) == 1:
					path_details[node] = [direction, percentage, orientations[1], pvalue]
					continue
				node = '%s|%i,%s|%i' % (loc[2], loc[3], loc[0], loc[1])
				if path_st.count(node) == 1:
					path_details[node] = [direction, percentage, orientations[1][::-1], pvalue]
					
			all_paths.append([path_st, path_details, HGT_part, part_cov])
	
	return all_paths	

def output_results(HGTs, options, samples):
	sys.stdout.write('Outputting HGT results...\n')
	output_dir = '%s/HGTs/' % options.outdir
	if not os.path.exists(output_dir): os.mkdir(output_dir)
	for s1, s2 in list(combinations(samples, 2)):
		N = 0
		outfile = '%s/%s.vs.%s.HGTs' % (output_dir, s1, s2)
		ofh = open(outfile, 'wb')
		sample_key = tuple(sorted([s1, s2]))
		for c, s, e, sample1, sample2, sample_HGTs in HGTs[sample_key]:
			if len(sample_HGTs) < 1: continue
			ofh.write('//%s|%i-%i\n' % (c, s, e))
			for path_st, details, HGT_part, part_cov in sample_HGTs:
				ofh.write('Path: %s\n' % path_st)
				ofh.write('Samples: %s\t%s\n' % (sample1, sample2))
				ofh.write('HGT parts: %s\n' % '\t'.join(HGT_part))
				cov_st = ['%s|%.6f,%.6f' % (node, part_cov[node][0], part_cov[node][1]) for node in part_cov]
				ofh.write('Coverage: %s\n' % '\t'.join(cov_st))
				for seg in details:
					direction, perc, (o1, o2), pvalue = details[seg]
					orientation = o1+o2
					ofh.write('Details: %s\t%s\t%s\t%.6f\t%.6f\n' % (seg, direction, orientation, perc, pvalue))
			N+=1
		ofh.close()
		ss1, ss2 = sample_key
		sys.stdout.write('    [%s vs %s] %i HGTs output in %s!\n' % (ss1, ss2, N, outfile))
	sys.stdout.write('All HGTs are formatted and output at %s.\n' % output_dir)	
	return 0

########################### main #########################		
def main(argv = sys.argv[1:]):

	####################### Arguments parser here ##########################
	parser = OptionParser(usage = USAGE, version="Version: " + __version__)
	
	# Required arguments
	requiredOptions = OptionGroup(parser, "Required options",
		"These options are required to run metaHGT, and may be supplied in any order.")
	
	requiredOptions.add_option("-c", "--conf", type = "string", metavar = "FILE",
		help = "The project configuration file.")
	
	requiredOptions.add_option("-o", "--outdir", type = "string", metavar = "DIR",
		help = "The output directory of metaHGT. If it doesn't exist, metaHGT will create it.")
						
	parser.add_option_group(requiredOptions)

	# Optional arguments that need to be supplied if not the same as default
	optOptions = OptionGroup(parser, "Optional parameters",
						"These options are optional, and may be supplied in any order.")

	optOptions.add_option("--samtools", type = "string", default = "samtools", metavar = "FILE",
							help = "the executable of Samtools if not already in ENV [default: samtools].\
								Samtools reference page: http://www.htslib.org/doc/samtools.html")
	
	optOptions.add_option("-t", "--nproc", type = "int", default = 1, metavar = 'INT',
							help = "Number of processor for metaHGT to use [default: 1].")
	
	optOptions.add_option("-l", "--min_length", type = "int", default = 500, metavar = 'INT',
							help = "Minimum segment length requirement for metaHGT to infer HGTs on[default: 500].")
	
	optOptions.add_option("-e", "--min_perc", type = "int", default = 50, metavar = 'INT',
							help = "Minimum percentage of HGT genotype in population to detect[default: 50].")
	
	optOptions.add_option("-p", "--pvalue", type = "int", default = 0.2, metavar = 'FLOAT',
							help = "Maximum q-value of HGT inference after FDR correction using Benjamini-Hochberg method [default: 0.05].")
							
	parser.add_option_group(optOptions)
	
	(options, args) = parser.parse_args(argv)

	if options.nproc < 0:
		sys.stderr.write('[WARNING]: Cannot set number of CPUs to be %i as you supplied, will use 1 CPU instead.\n' % options.nproc)
		options.nproc = 1
	elif options.nproc > mp.cpu_count():
		sys.stderr.write('[WARNING]: Cannot set number of CPUs to be %i, which exceeds the max number of CPUs available (%i), \
			will use %i CPUs instead.\n' (options.nproc, mp.cpu_count(), mp.cpu_count()))
		options.nproc = mp.cpu_count()
	
	
	if options.min_length < 500:
		parser.error("[FATAL]: minimum contig length has to be larger than 1Kbp.\n")
		exit(1)
	
	if options.min_perc < 1 or options.min_perc >100:
		parser.error("[FATAL]: minimum HGT percentage in population has to be an integer within range 1-100.\n")
		exit(1)
	
	if options.pvalue <= 0 or options.pvalue > 0.5:
		parser.error("[FATAL]: P-value should be a float within range (0.0, 0.5].\n")
		exit(1)
	
	if options.outdir == None:
		parser.error("[FATAL]: you must supply the output directory.\n")
		exit(1)
	elif not os.path.exists(options.outdir):
		try: os.mkdir(options.outdir)
		except:
			sys.stderr.write("[FATAL]: Error in creating output directory you supplied: %s.\n" % options.outdir)
			exit(1)
	
	# check if samtools in place
	if options.samtools == None or which(options.samtools) == None:
		sys.stderr.write("[FATAL]: Cannot locate samtools, please use \'--samtools\' to specify the executable location.\n")
		exit(1)
	else:
		options.samtools = which(options.samtools)
	
	# check configuration file to see if everything is OK
	assembly, binfile, depthfile, samples, bamfiles = parse_conf(options.conf)
	
	############################# Project kickstart here #################################
	# print basic project info
	sys.stdout.write("\n%s metaHGT version: %s %s\n\n" % ('='*30, __version__, '='*30))
	sys.stdout.write("Everything looks good, below is the summary of project:\n\n")
	sys.stdout.write("  Assembled contigs: %s\n" % assembly)
	if binfile != '': sys.stdout.write("  Population bin assignment: %s\n" % binfile)
	sys.stdout.write("  Contig coverage file: %s\n" % depthfile)
	sys.stdout.write("\n  Samples and associated BAM files:\n")
	for sample in samples:
		sys.stdout.write("      [%s] %s\n" % (sample, bamfiles[sample]))
	sys.stdout.write("\n  Output directory:\n    %s\n" % os.path.abspath(options.outdir))
	sys.stdout.write("\n  Parameters:\n")
	sys.stdout.write("    Minimum HGT segment length to detect: %i bp\n" % options.min_length)
	sys.stdout.write("    Minimum population percentage with HGT: %i%%\n" % options.min_perc)
	sys.stdout.write("    P-value threshold: %.4f\n" % options.pvalue)
	sys.stdout.write("    Number of threads: %i\n\n" % options.nproc)
	sys.stdout.write("\nmetaHGT started at %s\n" % (ctime()))
	sys.stdout.write("%s\n\n" % ('*'*50))
	
	####################### Shortcut for HGT inference ################################
	HGT_graph_pkl = os.path.join(options.outdir, 'HGT_graphs.pkl')
	if os.path.exists(HGT_graph_pkl):
		HGT_candidate_graphs = cPickle.load(open(HGT_graph_pkl, 'rb'))
		all_HGTs = {}
		for G, sample_key, s1, s2, c, s, e in HGT_candidate_graphs:
			if len(G.nodes()) == 0: continue
			if sample_key not in all_HGTs: all_HGTs[sample_key] = []
			HGTs = graph_HGT(G, options.min_perc, options.pvalue)
			if len(HGTs) == 0: continue
			all_HGTs[sample_key].append([c, s, e, s1, s2, HGTs])
		# output results
		output_results(all_HGTs, options, samples)
		sys.stdout.write("\nmetaHGT finished at %s\n" % (ctime()))
		return 0

	####################### Main data loading and serialization ##########################
	
	# load data
	# load length of contigs into dict
	len_dict = {}
	for record in SeqIO.parse(assembly, 'fasta'):
		len_dict[record.name] = len(record.seq)
	
	# estimate lib sizes
	sys.stdout.write('Estimating the lib insert size...\n')
	lib_property_pkl = os.path.abspath(options.outdir+'/lib_property.pkl')
	if not os.path.exists(lib_property_pkl):
		lib_property = estimate_lib(samples, bamfiles, len_dict)
		cPickle.dump(lib_property, open(lib_property_pkl, 'wb'))
	else:
		lib_property = cPickle.load(open(lib_property_pkl, 'rb'))
	for sample in lib_property:
		s, sd, rlen = lib_property[sample]
		sys.stdout.write('      [%s] Read length: %ibp, insert avg. size: %.2fbp, std: %.2fbp\n' % (sample, rlen, s, sd))
	sys.stdout.write('Done lib insert size estimation. [%s]\n\n' % (ctime())) 
	
	# get contig coverage
	sys.stdout.write('Loading contig coverage and variance...\n')
	coverage_pkl = os.path.abspath(options.outdir+'/contig_coverage.pkl')
	if not os.path.exists(coverage_pkl):
		contig_coverage_df, contig_variance_df = get_coverage(depthfile, samples)
		cPickle.dump([contig_coverage_df, contig_variance_df], open(coverage_pkl, 'wb'))
	else:
		contig_coverage_df, contig_variance_df = cPickle.load(open(coverage_pkl, 'rb'))
	sys.stdout.write('Done loading contig coverage and variance. [%s]\n\n' % (ctime()))
	
	# load bin information if available
	sys.stdout.write('Obtaining the contig bin assignments...\n')
	bin_assignment_pkl = os.path.abspath(options.outdir+'/bin_assignment.pkl')
	if not os.path.exists(bin_assignment_pkl):
		bin_dict = {}
		contig_bin_dict = {}
		if not os.path.exists(binfile):
			for contig in len_dict: contig_bin_dict[contig] = 'NA'
			bin_dict['NA'] = len_dict.keys()	
		if os.path.exists(binfile):			
			for line in open(binfile, 'rb'):
				contig, assignment = line.rstrip().split('\t')
				if assignment not in bin_dict: bin_dict[assignment] = []
				contig_bin_dict[contig] = assignment
				bin_dict[assignment].append(contig)
			bin_dict['NA'] = [contig for contig in len_dict.keys() if contig not in contig_bin_dict]
		cPickle.dump([bin_dict, contig_bin_dict], open(bin_assignment_pkl, 'wb'))
		sys.stdout.write('Contig bin assignments initialized.\n\n')
	else:
		bin_dict, contig_bin_dict = cPickle.load(open(bin_assignment_pkl, 'rb'))
	sys.stdout.write('Done loading bin assignment information. [%s]\n\n' % (ctime()))
	
	# load PE info
	sys.stdout.write('Identifying abnormal PE linkages and mappings...\n')
	PE_info_pkl = os.path.abspath(options.outdir+'/PE_info.pkl')
	if not os.path.exists(PE_info_pkl):
		# scan for stretched, missing, or contig-linking PEs
		sys.stdout.write('    Reading BAM files....\n')
		# init reference dict for fast lookups
		reference_dict = {} # hold the reference/contig tid-nodeID dict info
		bam = pysam.AlignmentFile(bamfiles[samples[0]], 'rb')
		for record in SeqIO.parse(assembly, 'fasta'):
			reference_dict[bam.gettid(record.name)] = record.name
		bam.close()
		
		# parallelize the whole process
		PE_info_cmds = []
		PE_info_dir = '%s/PE_info.tmp' % os.path.abspath(options.outdir)
		if not os.path.exists(PE_info_dir): os.mkdir(PE_info_dir)
		for sample in samples:
			out_pkl = '%s/%s.PE_info.pkl' % (PE_info_dir, sample)
			if not os.path.exists(out_pkl):
				PE_info_cmds.append([bamfiles[sample], out_pkl, len_dict, contig_bin_dict, reference_dict, lib_property, sample])	
		
		if len(PE_info_cmds) > 0:
			pool = mp.Pool(options.nproc)
			pool.map_async(run_PE_info_prep, PE_info_cmds)
			pool.close()
			pool.join()	
		
		#for cmd in PE_info_cmds: run_PE_info_prep(cmd)
		sys.stdout.write('    Done. Now merging PE info and serializing results...\n')
		
		# aggregate results
		agg_regions, agg_linkages = aggregate_info(samples, options)
		
		# serializing results
		sys.stdout.write('    Serializing results....\n')		
		cPickle.dump([agg_regions, agg_linkages], open(PE_info_pkl, 'wb'))
		shutil.rmtree(PE_info_dir)  # clean temp dir
		
	else: # load from pickle
		agg_regions, agg_linkages = cPickle.load(open(PE_info_pkl, 'rb'))
	sys.stdout.write('Done loading the PE information! [%s]\n\n' % (ctime()))
	
	############################### HGT inference here ###############################
	# iterate through the edges that connect blocks, use coverage and PE reads to infer
	# HGT direction and population percentage
	sys.stdout.write('Now inferring HGTs...\n')
	# select candidates for all possible combinations of samples
	# per sample per contig coverage dict
	sys.stdout.write('    Estimate contig coverage and sample coverage ratios...\n')
	# estimate sample coverage ratios
	coverage_ratio_pkl = os.path.join(options.outdir, 'coverage_ratio.pkl')
	if not os.path.exists(coverage_ratio_pkl):
		coverage_ratios = {}
		for s1, s2 in list(combinations(samples, 2)):	
			x = []
			coverage_ratios[(s1, s2)] = []
			for contig in contig_coverage_df.index:
				L = len_dict[contig]
				S = contig_coverage_df.ix[contig, s1]+contig_coverage_df.ix[contig, s2]
				try: R = contig_coverage_df.ix[contig, s1]/S
				except: continue
				# weighted ratio over contig length
				for i in range(L/500): coverage_ratios[(s1, s2)].append(R) # remove nan values
		cPickle.dump(coverage_ratios, open(coverage_ratio_pkl, 'wb'))
	else:
		coverage_ratios = cPickle.load(open(coverage_ratio_pkl, 'rb'))
	sys.stdout.write('    Done! [%s]\n' % (ctime()))
		
		
	# test all the linkages 
	# get all the segs and measure their coverage, judge if involved in HGT
	sys.stdout.write('    Constructing HGT graph for each sample combination...\n')
	xloc_pkl = os.path.join(options.outdir, 'xloc.pkl')
	if not os.path.exists(xloc_pkl):
		x_locs = {}  # store all the breakpoints
		x_samples = {}
		for c1, c2, info in agg_linkages.edges(data = True):
			c1, loc1, c2, loc2 = info['locs']
			max_count = max([info['details'][sample]['count'] for sample in samples])
			if max_count < 5: continue  # filter out the weak connections
			cov_df1 = contig_coverage_df.ix[c1, :]
			cov_df2 = contig_coverage_df.ix[c2, :]
			valid_samples1 = cov_df1[cov_df1 >= 5].index
			valid_samples2 = cov_df1[cov_df2 >= 5].index
			valid_samples = set(valid_samples1).intersection(set(valid_samples2))
			if len(valid_samples) >= 2:
				if c1 not in x_locs: x_locs[c1] = []
				if loc1 not in x_locs[c1]: x_locs[c1].append(loc1)
				x_samples[c1] = valid_samples1
				x_samples[c2] = valid_samples2
		sys.stdout.write('         X-locs and X-samples initiated.\n')
	
		sys.stdout.write('         Fine segment coverage calculation for X-locs and X-samples.\n')
		# recalculate the segment coverage
		# re-open all BAM files to get accurate segment Coverage
		bams = {}
		for sample in samples: bams[sample] = pysam.AlignmentFile(bamfiles[sample], 'rb')
		segment_property = {}
		segment_coverage = {}
		for c_ind, c in enumerate(x_locs.keys(), 1):  # c is a contig ID
		#	if c_ind % 1000 == 0: print '%i/%i contigs finished' % (c_ind, len(x_locs))
			locs = x_locs[c]
			length = len_dict[c]
			locs.insert(0, 1)
			locs.insert(len(locs), length)
			locs = sorted(locs)
			valid_samples = x_samples[c]
			segment_property[c] = {}
			segment_coverage[c] = {}
			for s, e in zip(locs[:-1], locs[1:]):
				if e-s+1 < options.min_length: continue
				# get per sample per segment coverage for the vaid samples
				segment_coverage[c][(s, e)] = dict(zip(valid_samples, [cal_segment_coverage(bams[sample], 
													lib_property[sample], c, s, e) for sample in valid_samples]))
		for sample in samples: bams[sample].close()
		cPickle.dump([x_locs, x_samples, segment_coverage], open(xloc_pkl, 'wb'))
	else:
		x_locs, x_samples, segment_coverage = cPickle.load(open(xloc_pkl, 'rb'))
	sys.stdout.write('    Done! [%s]\n' % (ctime()))

	sys.stdout.write('    Defining each segments property...\n')
	segment_property_pkl = os.path.join(options.outdir, 'segment_property.pkl')
	sample_mean = {}
	sample_std = {}
	for s1, s2 in list(combinations(samples, 2)):
		try: 
			R = coverage_ratios[(s1, s2)]
		except:
			R = coverage_ratios[(s2, s1)]
		R_clean = [ratio for ratio in R if not math.isnan(ratio)] # remove nan values
		mean = np.mean(R_clean)
		std = np.std(R_clean)
		sample_mean[tuple(sorted([s1, s2]))] = mean
		sample_std[tuple(sorted([s1, s2]))] = std
		
	if not os.path.exists(segment_property_pkl):
		segment_property = {}
		for c_ind, c in enumerate(segment_coverage,1):
			valid_samples = x_samples[c]
			#if c_ind % 100 == 0: print "%i/%i contigs processed " % (c_ind, len(segment_coverage))	
			segment_property[c] = {}
			# test against background coverage ratio, decide if 1. anchor, or 2. HGT frag
			for s, e in segment_coverage[c]:
				segment_property[c][(s, e)] = {}
				for s1, s2 in list(combinations(valid_samples, 2)):
					ind = 0
					try:
						mean = sample_mean[(s1, s2)]
						std = sample_std[(s1, s2)]
					except:
						mean = sample_mean[(s2, s1)]
						std = sample_std[(s2, s1)]
						ind = 1
					cov_counts = [int(segment_coverage[c][(s, e)][s1]),
								 int(segment_coverage[c][(s, e)][s2])]
					if ind == 1: cov_counts[0], cov_counts[1] = cov_counts[1], cov_counts[0]
					S = sum(cov_counts)
					try:simu_num = [int(S*mean), S-int(S*mean)]
					except: simu_num = [0, 0]
					odds_ratio, p = stats.fisher_exact([cov_counts, simu_num])
					# make judgement
					if odds_ratio > 1: odds_ratio = 1./odds_ratio
					if 1-odds_ratio > (0.75*options.min_perc)/100 and p < 0.05: # pre-filter for perc and p-val
						segment_property[c][(s, e)][(s1, s2)] = ['H', cov_counts, simu_num, ind]
					else:
						segment_property[c][(s, e)][(s1, s2)] = ['B', cov_counts, simu_num, ind]
					
		cPickle.dump(segment_property, open(segment_property_pkl, 'wb'))	
	else:
		segment_property = cPickle.load(open(segment_property_pkl, 'rb'))
	sys.stdout.write('    Done! [%s]\n' % (ctime()))
	
	############################# FINAL HGT INFERENCE #######################
	sys.stdout.write('    Individual subgraph disentangling, cores are HGT candidates...\n')
	HGT_candidate_graphs = []
	for c in segment_property:
		valid_samples = x_samples[c]
		for s, e in segment_property[c]:
			for s1, s2 in segment_property[c][(s, e)]:
				if s1 not in valid_samples or s2 not in valid_samples: continue
				sample_key = tuple(sorted([s1, s2]))
				type = segment_property[c][(s, e)][(s1, s2)][0]
				if type == 'B': continue
				G = nx.Graph() # holds individual graph with directed edges
				for neighbor in agg_linkages.neighbors(c):
					if neighbor not in segment_property: continue
					neighbor_valid_samples = x_samples[neighbor]
					if s1 not in neighbor_valid_samples or s2 not in valid_samples: continue
					edge_info = agg_linkages[c][neighbor]
					for edge_info_val in edge_info.values():
						edge_locs = edge_info_val['locs']
						edge_details = edge_info_val['details']
						neighbor_ind = edge_locs.index(neighbor)
						base_ind = edge_locs.index(c)
						neighbor_s = edge_locs[neighbor_ind+1]
						neighbor_e = find_other_loc(segment_coverage[neighbor], neighbor_s)
						if neighbor_e == -1: continue
						if neighbor_s > neighbor_e: neighbor_s, neighbor_e = neighbor_e, neighbor_s
						s1_count = edge_details[s1]['count']
						s2_count = edge_details[s2]['count']
						if max([s1_count, s2_count]) < 5: continue
						s1_orientation = edge_details[s1]['orientation']
						s2_orientation = edge_details[s2]['orientation']
						if base_ind != 0:
							s1_orientation = s1_orientation[::-1]
							s2_orientation = s2_orientation[::-1]
						try:
							s1_coverage = get_cov(segment_coverage, c, s, e, s1)
							s2_coverage = get_cov(segment_coverage, c, s, e, s2)
							s1_neighbor_coverage = get_cov(segment_coverage, neighbor,
													neighbor_s, neighbor_e, s1)
							s2_neighbor_coverage = get_cov(segment_coverage, neighbor,
													neighbor_s, neighbor_e, s2)
						except:
							continue
						try:
							neighbor_type = segment_property[neighbor][(neighbor_s, neighbor_e)][(s1, s2)][0]
						except KeyError:
							neighbor_type = segment_property[neighbor][(neighbor_s, neighbor_e)][(s2, s1)][0]
						
						
						# add nodes
						node11, node12 = (c, s), (c, e)
						node21, node22 = (neighbor, neighbor_s), (neighbor, neighbor_e)
						if node11 not in G:
							G.add_node(node11, covs = [s1_coverage, s2_coverage], type = type)
						if node12 not in G:
							G.add_node(node12, covs = [s1_coverage, s2_coverage], type = type)
						if node21 not in G:
							G.add_node(node21, covs = [s1_neighbor_coverage, s2_neighbor_coverage], type = neighbor_type)
						if node22 not in G:
							G.add_node(node22, covs = [s1_neighbor_coverage, s2_neighbor_coverage], type = neighbor_type)
						# add edges
						# add_within contig edges
						G.add_edge(node11, node12, type = 'contig')
						G.add_edge(node21, node22, type = 'contig')
						# add PE linked edges
						base_loc = edge_locs[base_ind+1]
						neighbor_loc = edge_locs[neighbor_ind+1]
						counts = [s1_count, s2_count]
						orientations = [edge_locs, s1_orientation, s2_orientation]
						if s == base_loc: node1 = node11
						else: node1 = node12
						if neighbor_s == neighbor_loc: node2 = node21
						else: node2 = node22
						G.add_edge(node1, node2, type = 'PE', counts = [0, 0], orientations = orientations)
						G[node1][node2]['counts'][0]+=counts[0]
						G[node1][node2]['counts'][1]+=counts[1]
				# disentangling Graph
				HGT_candidate_graphs.append([G, sample_key, s1, s2, c, s, e])
	# serialize results			
	cPickle.dump(HGT_candidate_graphs, open(HGT_graph_pkl, 'wb'))			
	sys.stdout.write('    Done. [%s]\n' % (ctime()))
	
	all_HGTs = {}
	for G, sample_key, s1, s2, c, s, e in HGT_candidate_graphs:
		if len(G.nodes()) == 0: continue
		if sample_key not in all_HGTs: all_HGTs[sample_key] = []
		HGTs = graph_HGT(G, options.min_perc, options.pvalue)
		if len(HGTs) == 0: continue
		all_HGTs[sample_key].append([c, s, e, s1, s2, HGTs])
	
	# output results
	output_results(all_HGTs, options, samples)
	
	sys.stdout.write("\nmetaHGT finished at %s\n" % (ctime()))
	return 0 
	
if __name__ == '__main__': main()


