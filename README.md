metaHGT
===========

metaHGT: detect horizontal gene transfers (HGTs) in series metagenomic dataset

One line pitcher
===========
metaHGT compares series metagenomes assemblies and PE reads mapping to infer possible HGT events that occurred between the times of sampling. 

Install
===========
use git:
	
	git clone https://luo-chengwei@bitbucket.org/luo-chengwei/metahgt.git

use hg:

	hg clone https://bitbucket.org/luo-chengwei/metahgt

You can also download the zip archive of it from bitbucket repository:

https://bitbucket.org/luo-chengwei/metahgt

Dependencies:
============

* Python-2.7 or above

+ Python libraries:

>BioPython

>Numpy 

>Scipy

>NetworkX

+ Third party pipelines:  

>Samtools v0.1.19+ (http://samtools.sourceforge.net/)

Note: older versions of these dependencies might work but it's not guaranteed.

You don't have to install it, just call ConStrains.py from wherever you put the whole folder. 

Usage
===========

The basic metaHGT analysis runs as below:

    metaHGT.py [options] -c/—conf <config.file> -o/—outdir <output directory>

The config file is a table delimited file, and the format follows:

    assembly    path/to/coassembly.fa
    depths    path/to/depth_file.txt
    sample1    path/to/sample_1.bam
    sample2    path/to/sample_2.bam
    ......

Note that the keywords "assembly" and "depths" are reserved. sample1,sample2 can be any name but "assembly" and "depths".

the assembly should be a coassembly fasta file; the depths file is a per contig file that follows format like this (tab delimited):

    #contig_name    contig_length    sample1_cov    sample1_cov_var    sample2_cov    sample2_cov_var    ....
    contig1    12033    1.342    2.3232    42.32    15.3283    ......
    contig2    58372    2.3483    3.232    4.23    1.438    ......
    ......
    
You can use the "jgi_summarize_bam_contig_depths" from MetaBAT package to get such depths file (https://bitbucket.org/berkeleylab/metabat)

The usage of jgi_summarize_bam_contig_depths is as below:

    Usage: jgi_summarize_bam_contig_depths <options> sortedBam1 [ sortedBam2 ...]

    where options include:
    --outputDepth       arg  The file to put the contig by bam depth matrix (default: STDOUT)
    --percentIdentity   arg  The minimum end-to-end % identity of qualifying reads (default: 97)
    --pairedContigs     arg  The file to output the sparse matrix of contigs which paired reads span (default: none)
    --unmappedFastq     arg  The prefix to output unmapped reads from each bam file suffixed by 'bamfile.bam.fastq.gz'
    --noIntraDepthVariance   Do not include variance from mean depth along the contig
    --showDepth              Output a .depth file per bam for each contig base
    --includeEdgeBases       When calculating depth & variance, include the 1-readlength edges (off by default)

    Options to control shredding contigs that are under represented by the reads
    --referenceFasta    arg  The reference file.  (It must be the same fasta that bams used)
    --shredLength       arg  The maximum length of the shreds
    --shredDepth        arg  The depth to generate overlapping shreds
    --minContigLength   arg  The mimimum length of contig to include for mapping and shredding
    --minContigDepth    arg  The minimum depth along contig at which to break the contig

For each sample, you should supply in the config file the indexed BAM file of reads from the sample mapped to the coassembly.

below is a sample config file:

    assembly    2009-10.megahit.fa
    depths    2009-10.depths.txt
    20090826    bams/20090826.vs.coassembly.bam
    20090828    bams/20090828.vs.coassembly.bam
    ......
    
Note that since we need the indexed BAM file, you should also have the .bai file in the same directory. For instance, in the above example:

sample 20090826's BAM file is "bams/20090826.vs.coassembly.bam", then you need to have "bams/20090826.vs.coassembly.bam.bai" as well.


Below is a detailed usage of metaHGT.py:

    Usage: metaHGT.py <required_parameters> [options]

    metaHGT: Horizontal Gene Transfer (HGT) detecting with series metagenomics

    Add --help to see a full list of required and optional arguments to run metaHGT.

    Additional information can also be found at:

    https://bitbucket.org/luo-chengwei/metaHGT/wiki

    If you use metaHGT in your work, please cite it as: <metaHGT citation TBD>

    Copyright: Chengwei Luo, Georgia Institute of Technology, 2015


    Options:
      --version             show program's version number and exit
      -h, --help            show this help message and exit

      Required options:
    These options are required to run metaHGT, and may be supplied in any order.

        -c FILE, --conf=FILE
                        The project configuration file.
        -o DIR, --outdir=DIR
                        The output directory of metaHGT. If it doesn't exist,
                        metaHGT will create it.

      Optional parameters:
        These options are optional, and may be supplied in any order.

        --samtools=FILE     the executable of Samtools if not already in ENV
                        [default: samtools].
                        Samtools reference page:
                        http://www.htslib.org/doc/samtools.html
        -t INT, --nproc=INT
                        Number of processor for metaHGT to use [default: 1].
        -l INT, --min_length=INT
                        Minimum segment length requirement for metaHGT to
                        infer HGTs on[default: 500].
        -e INT, --min_perc=INT
                        Minimum percentage of HGT genotype in population to
                        detect[default: 50].
        -p FLOAT, --pvalue=FLOAT
                        Maximum q-value of HGT inference after FDR correction
                        using Benjamini-Hochberg method [default: 0.05].


Tutorial
=====================

Below is an example to walk you through what a real metaHGT analysis should look like.

The project demostrates a case in which a K-12 E. coli genome (baseline) underwent in-silico LGT events from Salmonella typhi. The LGTs are ~1Kbp fragments from S. typhi genome and affect ~60% of the E. coli population.

You can run the following command to obtain the tutorial data:

    wget URL
    
or alternatively:

    curl URL

After you've downloaded the "metaHGT_tutorial.tar.bz2", you can untar the package using:

    tar -xjvf metaHGT_tutorial.tar.bz2

Then you should move to the directory by:

    cd metaHGT_tutorial
    
and then you can show the content of it by running:

    ls -lh ./
    
this will give you a list of files, similar to this:

    total 1624576
    -rw-r--r--  1 cluo  staff   4.5M Oct 29 00:25 LGT_contigs.fa
    -rw-r--r--  1 cluo  staff   199M Oct 29 00:41 LGT_vs_LGT.bam
    -rw-r--r--  1 cluo  staff    33K Oct 29 00:41 LGT_vs_LGT.bam.bai
    -rw-r--r--  1 cluo  staff   199M Oct 29 00:57 LGT_vs_baseline.bam
    -rw-r--r--  1 cluo  staff    20K Oct 29 00:57 LGT_vs_baseline.bam.bai
    -rw-r--r--  1 cluo  staff   4.4M Oct 29 00:09 baseline_contigs.fa
    -rw-r--r--  1 cluo  staff   193M Oct 29 00:37 baseline_vs_LGT.bam
    -rw-r--r--  1 cluo  staff    33K Oct 29 00:37 baseline_vs_LGT.bam.bai
    -rw-r--r--  1 cluo  staff   193M Oct 29 00:14 baseline_vs_baseline.bam
    -rw-r--r--  1 cluo  staff    20K Oct 29 00:14 baseline_vs_baseline.bam.bai
    -rw-r--r--@ 1 cluo  staff   208B Nov 28 16:52 conf.txt

In total, there are 2 fasta files, 4 bam files and their associated index files (*.bai), and 1 project configuration file, conf.txt




